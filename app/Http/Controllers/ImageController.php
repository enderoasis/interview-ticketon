<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ImageService;
use App\Image;

class ImageController extends Controller
{
 	protected $imageservice;

	public function __construct(ImageService $imageservice)
	{
		$this->imageservice = $imageservice;
	}

	public function create(Request $request)
    {
      
      $this->imageservice->create($request);

      return response()->json(['message'=>'Изображение загружено и успешно разрезано на 4 части']);
    }

    public function show($id)
    {
      
      $image_parts = Image::select('parts')->first()->parts;
 	  $parts_arr = json_decode($image_parts,true);     

      foreach ($parts_arr as $part) {
      	 if($part['part_no'] == $id){
      	 	return $part;
      	 }
      }
    }
}
