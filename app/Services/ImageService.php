<?php

namespace App\Services;

use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use App\Image;
use Intervention\Image\ImageManagerStatic as ImageTool;
use File;

class ImageService
{
	public function __construct(ImageRepository $image)
	{
		$this->image = $image;
	}

    public function create(Request $request)
	{
       
    $extension = $request->file('image')->getClientOriginalExtension();
    $image_name = $request->file('image')->getClientOriginalName();
    $image = ImageTool::make($request->file('image'));

    $height = $image->height();
    $width = $image->width();

    if($width > 500) {
        $image = $image->fit(500);
    } 

    $image->save(public_path('images/'. $image_name));

    $image = ImageTool::make(public_path('images/'. $image_name));

    $height = $image->height();
    $width = $image->width();

    // Вычисление высоты и ширины каждой части, так как мы делим картинку на 4 части, каждая часть в рамках 2х2 сетки занимает 1/2 высоты и ширины.

    $puzzlePieceHeight = $height / 2;
    $puzzlePieceWidth = $width / 2;

    $part = 1;

    $images = collect([]);

    // Использую высоту и ширину мы пробегаемся по абциссе и ординате двумя циклами, для обрезки каждой части.
    for ($y=0; $y <=1 ; $y++) 
    {
        for ($x=0; $x <= 1; $x++) 
        {

            $xOffset = ceil($puzzlePieceWidth * $x);
            $yOffset = ceil($puzzlePieceHeight * $y);

            $partImg = ImageTool::make(public_path('images/'. $image_name))
		                ->crop(
		                 ceil($puzzlePieceWidth),
		            	 ceil($puzzlePieceHeight),
		            	 $xOffset,$yOffset);

            $partFileName = 'part' . $part . '.' . $extension;
            $partImg->save(public_path('images/'. $partFileName));
            $images->add([ 'image_url' => public_path('images/'. $partFileName),  'part_no' => $part++ ]);

        }
    }


        Image::updateOrCreate(
        [
         'id' => 1
       	],
        [
        'original' => public_path('images/'. $request->file('image')->getClientOriginalName()),
        'parts' => $images
        ]);
	}
}