#!/bin/bash

composer install --optimize-autoloader --no-dev
php artisan key:generate

sudo chmod 777 -R storage
sudo chmod 777 -R bootstrap

php artisan migrate:fresh --force

php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan view:clear


