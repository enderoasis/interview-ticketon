## Interview-ticketon

Technical interview on Ticketon.kz. Position: Junior PHP Developer. Deadline: 48 hours.

# Description

Create web service which accepts image and makes a puzzle of 4 parts from it. Each part should be stored in database and be accessable by API requests. Service must be wrapped by Docker.

# Docker

Edit your .env file and run: 

docker-compose build app

# Bash script

Run ./deploy in console

# Postman Import Collection

Import  **interview-ticketon.postman_collection.json**  to your Postman programm. To see and test API requests
